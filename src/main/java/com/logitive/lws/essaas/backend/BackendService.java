package com.logitive.lws.essaas.backend;

import java.io.IOException;
import java.util.List;

import com.logitive.lws.lwsi.app.model.Feed;
import com.logitive.lws.lwsi.app.model.Report2DLine;
import com.logitive.lws.lwsi.app.model.Report3DLine;
import com.logitive.lws.lwsi.app.model.SearchQuery;
import com.logitive.lws.lwsi.app.model.feed.DataFeed;
import com.logitive.lws.lwsi.app.model.result.SearchResult;
import com.logitive.lws.lwsi.app.model.store.Store;
import com.logitive.lws.lwsi.app.model.user.User;
import com.logitive.lws.lwsi.exception.EmailExistException;
import com.logitive.lws.lwsi.exception.LWSValidationException;
import com.logitive.lws.lwsi.exception.SystemException;

public interface BackendService {

	public abstract Long updatefeed(String clId)throws SystemException;
	public abstract Feed getFeed(String clId);
	public abstract DataFeed getDataFeed(String clId);
	// Report
	public abstract List<Report2DLine> getReport1(String clId);
	public abstract List<Report3DLine> getReport2(String clId);
	public SearchResult searchResult(String clId,SearchQuery searchQuery)throws Exception;
	public long docCount(String clId)throws SystemException;
	public String saveFeed(String clId,Feed feed) throws SystemException;
	public String saveDataFeed(String clId,DataFeed dataFeed)throws SystemException;
	public Store getStore();
	public List<String> valueList(String clId,String facetName) throws SystemException;
	public long userCount(String clId);
	public long queryCount(String clId);
	public List<Report2DLine> getCurrentQuries(String clId);
	public User loginUser(String email,String password);
	public User getAdminUser(String clId);
	public String saveAdminUser(String clId,User user)throws SystemException;
	public List<User> getAllUserList();
	public User findUser(String email);
	public User register(String email,String password) throws EmailExistException, SystemException;
	public User recoverPassword(String email) throws LWSValidationException, IOException;
	public User activateUser(String registrationToken) throws SystemException;
	public void sendUserTokenByMail(String email) throws SystemException;

}