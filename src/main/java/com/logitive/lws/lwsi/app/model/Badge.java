package com.logitive.lws.lwsi.app.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;


public class Badge {
	private String id;
	@NotNull(message="Name may not be emty")
	private String name;
	@NotNull(message="an Icon must be selected")
	private String url;
	//private String position="position:relative; z-index: 2; top: 0%;left: 0%;transform: translate(0%, 0%)";
	private Position position=Position.CENTER;
	private List<KeyValue> keyValueList=new ArrayList<KeyValue>();
	
	public enum Position {
		TOP_LEFT,TOP_CENTER,TOP_RIGHT,
		LEFT,CENTER,RIGHT,
		BOTTOM_LEFT,BOTTOM_CENTER,BOTTOM_RIGHT,
	};

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<KeyValue> getKeyValueList() {
		return keyValueList;
	}

	public void setKeyValueList(List<KeyValue> keyValueList) {
		this.keyValueList = keyValueList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Badge other = (Badge) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
}
