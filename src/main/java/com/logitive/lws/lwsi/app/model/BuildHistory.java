package com.logitive.lws.lwsi.app.model;

import java.util.Date;

//@XmlRootElement
public class BuildHistory {
	private String id;
	private int docCount;
	private String user;
	private Date built;
	private Type type;
	public enum Type {
		XML, CSV
	}
	
	
	public BuildHistory(){
		// JAXB needs this
	}


	public BuildHistory(String id, int docCount, String user, Date built,
			Type type) {
		super();
		this.id = id;
		this.docCount = docCount;
		this.user = user;
		this.built = built;
		this.type = type;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public int getDocCount() {
		return docCount;
	}


	public void setDocCount(int docCount) {
		this.docCount = docCount;
	}


	public String getUser() {
		return user;
	}


	public void setUser(String user) {
		this.user = user;
	}


	public Date getBuilt() {
		return built;
	}


	public void setBuilt(Date built) {
		this.built = built;
	}


	public Type getType() {
		return type;
	}


	public void setType(Type type) {
		this.type = type;
	}
	
	
}
