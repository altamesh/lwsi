package com.logitive.lws.lwsi.app.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.Dependent;
import javax.inject.Named;

@Named
@Dependent
public class Feed implements Serializable {

	private static final long serialVersionUID = 76937934767434655L;
	private String id;
	private String clId;
	private List<Field> fieldList = new ArrayList<Field>();
	private List<Template> templateList = new ArrayList<Template>();
	private String stopWords;
	private List<Integer> itemCountPerPageList = new ArrayList<Integer>();
	private String synonyms;
	private int autocompKeypressCount;
	private int autocompWordCount;
	private int autocompItemCount;
	private String autocompTemplate;
	private List<SortItem> sortItemList = new ArrayList<SortItem>();
	private List<Icon> iconList = new ArrayList<Icon>();
	private List<Badge> badgeList = new ArrayList<Badge>();
	private List<Navigation> navigationList = new ArrayList<Navigation>();
	private List<QueryAction> queryActionList = new ArrayList<QueryAction>();
	private List<Promotion> promotionList = new ArrayList<Promotion>();
	
	public Feed() {
		super();
	}

	public String getClId() {
		return clId;
	}

	public void setClId(String clId) {
		this.clId = clId;
	}

	public int getAutocompKeypressCount() {
		return autocompKeypressCount;
	}

	public void setAutocompKeypressCount(int autocompKeypressCount) {
		this.autocompKeypressCount = autocompKeypressCount;
	}

	public int getAutocompWordCount() {
		return autocompWordCount;
	}

	public void setAutocompWordCount(int autocompWordCount) {
		this.autocompWordCount = autocompWordCount;
	}

	public int getAutocompItemCount() {
		return autocompItemCount;
	}

	public void setAutocompItemCount(int autocompItemCount) {
		this.autocompItemCount = autocompItemCount;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Field> getFieldList() {
		return fieldList;
	}

	public void setFieldList(List<Field> fieldList) {
		this.fieldList = fieldList;
	}

	public List<Integer> getItemCountPerPageList() {
		return itemCountPerPageList;
	}

	public void setItemCountPerPageList(List<Integer> itemCountPerPageList) {
		this.itemCountPerPageList = itemCountPerPageList;
	}

	public List<SortItem> getSortItemList() {
		return sortItemList;
	}

	public void setSortItemList(List<SortItem> sortItemList) {
		this.sortItemList = sortItemList;
	}

	public String getStopWords() {
		return stopWords;
	}

	public void setStopWords(String stopWords) {
		this.stopWords = stopWords;
	}

	public String getSynonyms() {
		return synonyms;
	}

	public void setSynonyms(String synonyms) {
		this.synonyms = synonyms;
	}

	
	public String getAutocompTemplate() {
		return autocompTemplate;
	}

	public void setAutocompTemplate(String autocompTemplate) {
		this.autocompTemplate = autocompTemplate;
	}

	public List<Template> getTemplateList() {
		return templateList;
	}

	public void setTemplateList(List<Template> templateList) {
		this.templateList = templateList;
	}

	public List<Badge> getBadgeList() {
		return badgeList;
	}

	public void setBadgeList(List<Badge> badgeList) {
		this.badgeList = badgeList;
	}

	public List<Icon> getIconList() {
		return iconList;
	}

	public void setIconList(List<Icon> iconList) {
		this.iconList = iconList;
	}

	public List<Navigation> getNavigationList() {
		return navigationList;
	}

	public void setNavigationList(List<Navigation> navigationList) {
		this.navigationList = navigationList;
	}

	public List<QueryAction> getQueryActionList() {
		return queryActionList;
	}

	public void setQueryActionList(List<QueryAction> queryActionList) {
		this.queryActionList = queryActionList;
	}

	public List<Promotion> getPromotionList() {
		return promotionList;
	}

	public void setPromotionList(List<Promotion> promotionList) {
		this.promotionList = promotionList;
	}

}