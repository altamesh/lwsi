package com.logitive.lws.lwsi.app.model;

import java.util.Date;


public class Field {
	private String id;
	private Date updated;
	private String name;
	// facet
	private boolean isFacet;
	private boolean isSingleSelect;
	private String facetCaption;
	private FacetSortType facetSortType=FacetSortType.FREQUENCY_ASC;
	private long facetLimit;
	private String hierarchieDelimiter;
	private String facetCustomSort;
	private String facetBucket;
	// sorting
	private boolean isAscSortable;
	private int sortAscPos;// ?
	private String ascLabel;

	private boolean isDescSortable;
	private int sortDescPos;// ?
	private String descLabel;
	// compare
	private boolean isCompareable;
	private int comparePos;
	private String compareLabel;
	private Type type;
	private boolean isTokenized;
	
	
	public boolean isTokenized() {
		return isTokenized;
	}

	public boolean getIsTokenized() {
		return isTokenized;
	}
	public void setTokenized(boolean isTokenized) {
		this.isTokenized = isTokenized;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public enum Type {
		TEXT, NUMBER
	};

	public enum FacetSortType {
		VALUE_ASC, VALUE_DESC, FREQUENCY_ASC, FREQUENCY_DESC, CUSTOM
	}
	public Field(){
		
	}
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFacetCaption() {
		return facetCaption;
	}

	public void setFacetCaption(String facetCaption) {
		this.facetCaption = facetCaption;
	}

	public boolean isFacet() {
		return isFacet;
	}

	public void setFacet(boolean isFacet) {
		this.isFacet = isFacet;
	}

	
	public boolean isSingleSelect() {
		return isSingleSelect;
	}

	public void setSingleSelect(boolean isSingleSelect) {
		this.isSingleSelect = isSingleSelect;
	}

	public String getFacetBucket() {
		return facetBucket;
	}

	public void setFacetBucket(String facetBucket) {
		this.facetBucket = facetBucket;
	}

	public FacetSortType getFacetSortType() {
		return facetSortType;
	}

	public void setFacetSortType(FacetSortType facetSortType) {
		this.facetSortType = facetSortType;
	}

	

	public long getFacetLimit() {
		return facetLimit;
	}

	public void setFacetLimit(long facetLimit) {
		this.facetLimit = facetLimit;
	}

	public String getHierarchieDelimiter() {
		return hierarchieDelimiter;
	}

	public void setHierarchieDelimiter(String hierarchieDelimiter) {
		this.hierarchieDelimiter = hierarchieDelimiter;
	}

	public boolean isAscSortable() {
		return isAscSortable;
	}

	public void setAscSortable(boolean isAscSortable) {
		this.isAscSortable = isAscSortable;
	}

	public int getSortAscPos() {
		return sortAscPos;
	}

	public void setSortAscPos(int sortAscPos) {
		this.sortAscPos = sortAscPos;
	}

	public String getAscLabel() {
		return ascLabel;
	}

	public void setAscLabel(String ascLabel) {
		this.ascLabel = ascLabel;
	}

	public boolean isDescSortable() {
		return isDescSortable;
	}

	public void setDescSortable(boolean isDescSortable) {
		this.isDescSortable = isDescSortable;
	}

	public int getSortDescPos() {
		return sortDescPos;
	}

	public void setSortDescPos(int sortDescPos) {
		this.sortDescPos = sortDescPos;
	}

	public String getDescLabel() {
		return descLabel;
	}

	public void setDescLabel(String descLabel) {
		this.descLabel = descLabel;
	}

	public boolean isCompareable() {
		return isCompareable;
	}

	public void setCompareable(boolean isCompareable) {
		this.isCompareable = isCompareable;
	}

	public int getComparePos() {
		return comparePos;
	}

	public void setComparePos(int comparePos) {
		this.comparePos = comparePos;
	}

	public String getCompareLabel() {
		return compareLabel;
	}

	public void setCompareLabel(String compareLabel) {
		this.compareLabel = compareLabel;
	}

	public String getFacetCustomSort() {
		return facetCustomSort;
	}

	public void setFacetCustomSort(String facetCustomSort) {
		this.facetCustomSort = facetCustomSort;
	}

}