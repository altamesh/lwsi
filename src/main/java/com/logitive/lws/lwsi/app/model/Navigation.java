package com.logitive.lws.lwsi.app.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;


public class Navigation {
	private String id;
	@NotNull
	private String name;
	private boolean active;
	private String bannerText;
	private String bannerSrc;
	private String bannerTarget;
	private List<KeyValue> keyValueList = new ArrayList<KeyValue>();
	private Type type = Type.NO_BANNER;

	public enum Type {
		NO_BANNER("No Banner"), TEXT_BANNER("Text Banner"), IMAGE_BANNER("Image Banner");
		private String caption;

		Type(String caption) {
			this.caption=caption;
		}

		public String getCaption() {
			return caption;
		}

	};

	public String getBannerText() {
		return bannerText;
	}

	public void setBannerText(String bannerText) {
		this.bannerText = bannerText;
	}

	public String getBannerSrc() {
		return bannerSrc;
	}

	public void setBannerSrc(String bannerSrc) {
		this.bannerSrc = bannerSrc;
	}

	public String getBannerTarget() {
		return bannerTarget;
	}

	public void setBannerTarget(String bannerTarget) {
		this.bannerTarget = bannerTarget;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<KeyValue> getKeyValueList() {
		return keyValueList;
	}

	public void setKeyValueList(List<KeyValue> keyValueList) {
		this.keyValueList = keyValueList;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Navigation other = (Navigation) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
