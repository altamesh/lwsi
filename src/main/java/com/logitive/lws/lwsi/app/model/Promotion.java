package com.logitive.lws.lwsi.app.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;



public class Promotion {
	
	public enum Boost {
		SLIGHT,MODRATE, HEIGH,HEIGHTEST
	};
	private String id;
	@NotNull(message="Name may not be emty")
	private String name;
	private Boost boost=Boost.SLIGHT;
	private List<KeyValue> keyValueList=new ArrayList<KeyValue>();
	private Date startDate;
	private Date endDate;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Boost getBoost() {
		return boost;
	}
	
	public void setBoost(Boost boost) {
		this.boost = boost;
	}
	
	public List<KeyValue> getKeyValueList() {
		return keyValueList;
	}
	
	public void setKeyValueList(List<KeyValue> keyValueList) {
		this.keyValueList = keyValueList;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public Date getEndDate() {
		return endDate;
	}
	
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Promotion other = (Promotion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
