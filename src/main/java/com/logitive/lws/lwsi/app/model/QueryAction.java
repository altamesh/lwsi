package com.logitive.lws.lwsi.app.model;

import javax.validation.constraints.NotNull;


public class QueryAction {
	private String id;

	@NotNull
	private String query;
	private boolean active;
	@NotNull
	private String navigateTarget;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getQuery() {
		return query;
	}


	public void setQuery(String query) {
		this.query = query;
	}


	public boolean isActive() {
		return active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	public String getNavigateTarget() {
		return navigateTarget;
	}


	public void setNavigateTarget(String navigateTarget) {
		this.navigateTarget = navigateTarget;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QueryAction other = (QueryAction) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	};
	
}
