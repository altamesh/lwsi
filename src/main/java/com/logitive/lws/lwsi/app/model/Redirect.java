package com.logitive.lws.lwsi.app.model;

public class Redirect {
	private String id;
	private String query;
	private String text;
	public Redirect(String id, String query, String text) {
		super();
		this.id = id;
		this.query = query;
		this.text = text;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

}
