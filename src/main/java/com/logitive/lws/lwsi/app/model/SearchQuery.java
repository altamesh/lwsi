package com.logitive.lws.lwsi.app.model;

import java.util.ArrayList;
import java.util.List;

public class SearchQuery {

	private Integer start=new Integer(1);

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	private List<KeyValue> keyValueList=new ArrayList<KeyValue>();

	public List<KeyValue> getKeyValueList() {
		return keyValueList;
	}

	public void setKeyValueList(List<KeyValue> keyValueList) {
		this.keyValueList = keyValueList;
	}
	
}
