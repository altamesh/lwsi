package com.logitive.lws.lwsi.app.model.feed;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;



public class DataFeed implements Serializable {

	private static final long serialVersionUID = 76937934767434655L;

	
	private String id;
	@NotNull(message = "my not be null")
	private String url;
	private String username;
	private String password;
	private Date lastIndexed;
	private Date lastIndexedStarted;
	private String clId;

	public Date getLastIndexed() {
		return lastIndexed;
	}

	public void setLastIndexed(Date lastIndexed) {
		this.lastIndexed = lastIndexed;
	}
	//private List<DataField> fieldList;
	
	public DataFeed() {
		super();
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public Date getLastIndexedStarted() {
		return lastIndexedStarted;
	}

	public void setLastIndexedStarted(Date lastIndexedStarted) {
		this.lastIndexedStarted = lastIndexedStarted;
	}

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getClId() {
		return clId;
	}

	public void setClId(String clId) {
		this.clId = clId;
	}
	
}