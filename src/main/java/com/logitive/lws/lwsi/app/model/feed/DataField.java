package com.logitive.lws.lwsi.app.model.feed;

import java.util.Date;

public class DataField {
	private String id;
	private Date updated;
	private String name;
	private boolean isSearcgable;
	private Type type;

	public enum Type {
		TEXT_NON_SEARCHABLE, TEXT_SEARCHABLE, NUMBER
	}

	public DataField(String id, Date updated, String name,
			boolean isSearcgable, Type type) {
		super();
		this.id = id;
		this.updated = updated;
		this.name = name;
		this.isSearcgable = isSearcgable;
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public boolean isSearcgable() {
		return isSearcgable;
	}

	public void setSearcgable(boolean isSearcgable) {
		this.isSearcgable = isSearcgable;
	}

}