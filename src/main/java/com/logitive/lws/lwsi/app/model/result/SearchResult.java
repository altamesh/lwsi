package com.logitive.lws.lwsi.app.model.result;

import java.util.List;

public class SearchResult {

	private long count;
	private long start;
	private int pageSize;
	private String didYouMean;
	private String bannerSrc;
	private String bannerTarget;
	private String bannerText;
	private String redirect;
	private double priceMin;
	private double priceMax;
	private List<SearchResultItem> searchResultItemList;
	private List<SearchResultFilter> searchResultFilterList;

	public SearchResult() {

	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}
	
	public String getDidYouMean() {
		return didYouMean;
	}

	public void setDidYouMean(String didYouMean) {
		this.didYouMean = didYouMean;
	}

	public String getBannerSrc() {
		return bannerSrc;
	}

	public void setBannerSrc(String bannerSrc) {
		this.bannerSrc = bannerSrc;
	}

	
	public String getBannerTarget() {
		return bannerTarget;
	}

	public void setBannerTarget(String bannerTarget) {
		this.bannerTarget = bannerTarget;
	}

	public String getBannerText() {
		return bannerText;
	}

	public void setBannerText(String bannerText) {
		this.bannerText = bannerText;
	}

	public String getRedirect() {
		return redirect;
	}

	public void setRedirect(String redirect) {
		this.redirect = redirect;
	}

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	
	public double getPriceMin() {
		return priceMin;
	}

	public void setPriceMin(double priceMin) {
		this.priceMin = priceMin;
	}

	public double getPriceMax() {
		return priceMax;
	}

	public void setPriceMax(double priceMax) {
		this.priceMax = priceMax;
	}

	public List<SearchResultItem> getSearchResultItemList() {
		return searchResultItemList;
	}

	public void setSearchResultItemList(
			List<SearchResultItem> searchResultItemList) {
		this.searchResultItemList = searchResultItemList;
	}

	public List<SearchResultFilter> getSearchResultFilterList() {
		return searchResultFilterList;
	}

	public void setSearchResultFilterList(
			List<SearchResultFilter> searchResultFilterList) {
		this.searchResultFilterList = searchResultFilterList;
	}
}
