package com.logitive.lws.lwsi.app.model.result;

import java.util.ArrayList;
import java.util.List;

public class SearchResultFilter {

	private String name;
	private String label;

	private List<SearchResultFilterItem> searchResultFilterItemList=new ArrayList<SearchResultFilterItem>();
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public List<SearchResultFilterItem> getSearchResultFilterItemList() {
		return searchResultFilterItemList;
	}

	public void setSearchResultFilterItemList(
			List<SearchResultFilterItem> searchResultFilterItemList) {
		this.searchResultFilterItemList = searchResultFilterItemList;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	
	
}
