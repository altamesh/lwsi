package com.logitive.lws.lwsi.app.model.result;

public class SearchResultFilterItem {

	private String name;
	private long count;
	
	public SearchResultFilterItem() {
	}
	
	
	public SearchResultFilterItem(String name, long count) {
		super();
		this.name = name;
		this.count = count;
	}


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}
	
	
	
}
