package com.logitive.lws.lwsi.app.model.result;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;


public class SearchResultItem {
	private String id;
	private String title;
	private String url;
	private String image;
	private double price;
	
	private final static Logger LOGGER = Logger.getLogger(SearchResultItem.class
			.getName());
	private Map<String, Object> fieldMap = new ConcurrentHashMap<String, Object>();
	private List<SearchResultItemBadge> searchResultItemBadgeList = new ArrayList<SearchResultItemBadge>();

	
	public SearchResultItem(String id, String title, String url, String image,
			double price) {
		super();
		this.id = id;
		this.title = title;
		this.url = url;
		this.image = image;
		this.price = price;
	}

	public SearchResultItem() {
		super();
	}

	public Map<String, Object> getFieldMap() {
		return fieldMap;
	}

	public void setFieldMap(Map<String, Object> fieldMap) {
		this.fieldMap = fieldMap;
	}

	public void addField(String key, Object value) {
		fieldMap.put(key, value);
	}

	public String getField(String key) {
		Set<String> set=fieldMap.keySet();
		LOGGER.info(set.toString());
		Object ob = fieldMap.get(key);
		return ob.toString();
	}

	public List<SearchResultItemBadge> getSearchResultItemBadgeList() {
		return searchResultItemBadgeList;
	}

	public void setSearchResultItemBadgeList(
			List<SearchResultItemBadge> searchResultItemBadgeList) {
		this.searchResultItemBadgeList = searchResultItemBadgeList;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getImage() {
		
		return image.replaceFirst("https://res.cloudinary.com/the-cools/image/upload/", "https://res.cloudinary.com/the-cools/image/upload/t_PRODUCT/");
	}

	public void setImage(String image) {
		this.image = image;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	
	

}
