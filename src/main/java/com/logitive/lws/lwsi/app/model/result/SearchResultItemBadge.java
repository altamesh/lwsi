package com.logitive.lws.lwsi.app.model.result;

public class SearchResultItemBadge {

	private String name;
	private String url;
	private int x;
	private int y;

	public SearchResultItemBadge(String name, String url, int x, int y) {
		super();
		this.name = name;
		this.url = url;
		this.x = x;
		this.y = y;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

}
