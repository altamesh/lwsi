package com.logitive.lws.lwsi.app.model.store;

import java.util.Date;
import java.util.List;


public class Store{
	private String id;
	private Date lastModified;
	private List<StoredTemplate> storedTemplateList;
	private List<StoredIcon> storedIconList;
	private List<StoredColorIcon> storedColorIconList;
	private List<StoredBadge> storedBadgeList;

	public List<StoredColorIcon> getStoredColorIconList() {
		return storedColorIconList;
	}

	public void setStoredColorIconList(List<StoredColorIcon> storedColorIconList) {
		this.storedColorIconList = storedColorIconList;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id=id;		
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified=lastModified;		
	}

	public List<StoredTemplate> getStoredTemplateList() {
		return storedTemplateList;
	}

	public void setStoredTemplateList(List<StoredTemplate> storedTemplateList) {
		this.storedTemplateList = storedTemplateList;
	}

	public List<StoredIcon> getStoredIconList() {
		return storedIconList;
	}

	public void setStoredIconList(List<StoredIcon> storedIconList) {
		this.storedIconList = storedIconList;
	}

	public List<StoredBadge> getStoredBadgeList() {
		return storedBadgeList;
	}

	public void setStoredBadgeList(List<StoredBadge> storedBadgeList) {
		this.storedBadgeList = storedBadgeList;
	}
}
