package com.logitive.lws.lwsi.app.model.store;



public class StoredTemplate {
	private String id;
	private String name;
	private String text;
	private Source source;
	
	public enum Source {
		SEARCH_RESULT,AUTO_COMPLETE,RECENTLY_VIEWED,RECOMMENDATION
	};
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Source getSource() {
		return source;
	}
	public void setSource(Source source) {
		this.source = source;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
}
