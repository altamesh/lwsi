package com.logitive.lws.lwsi.app.model.user;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;


public class User {

	private String id;
	private String clId;
	private String email;
	private String password;
	private String name;
	private boolean admin;
	private boolean canGoLive;
	private boolean updateIndex;
	private String rememberMe;
	private String token;
	
	@Enumerated(EnumType.STRING)
	private State state = State.EMAIL_UNCONFIRMED;

	public enum State {
		EMAIL_UNCONFIRMED,ACTIVE, BLOCKED
	}	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClId() {
		return clId;
	}

	public void setClId(String clId) {
		this.clId = clId;
	}

	public boolean isCanGoLive() {
		return canGoLive;
	}

	public void setCanGoLive(boolean canGoLive) {
		this.canGoLive = canGoLive;
	}

	public boolean isUpdateIndex() {
		return updateIndex;
	}

	public void setUpdateIndex(boolean updateIndex) {
		this.updateIndex = updateIndex;
	}

	
	public String getRememberMe() {
		return rememberMe;
	}

	public void setRememberMe(String rememberMe) {
		this.rememberMe = rememberMe;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
}
