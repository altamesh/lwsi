package com.logitive.lws.lwsi.exception;

public class SystemException extends RuntimeException {

	private Exception exception;
	public Exception getException() {
		return exception;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SystemException(Exception exception) {
		this.exception = exception;
	}
	
	
}
